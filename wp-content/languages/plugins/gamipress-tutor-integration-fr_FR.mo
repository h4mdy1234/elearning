��    #      4  /   L           	       &   -  "   T     w     �     �     �  &   �  "   �           7     N  !   c     �     �     �     �      �     �          "  	   3  !   =  h   _     �     �     �  -     !   =  	   _     i       :   �  =  �          !  ,   5     b     �     �     �     �  7   �  *   &	     Q	     f	     |	  #   �	     �	  $   �	     �	     
  ,   
     L
     l
     �
  	   �
  !   �
  n   �
     ;     X     l  )   }     �  	   �     �     �  :   �                	                                            #   "            
          !                                                                        Complete a course Complete a lesson Complete a lesson of a specific course Complete a lesson of the course %s Complete a quiz Complete a specific course Complete a specific lesson Complete a specific quiz Complete any quiz of a specific course Complete any quiz of the course %s Complete the course %s Complete the lesson %s Complete the quiz %s Connect GamiPress with Tutor LMS. Enroll a course Enroll a specific course Enroll the course %s Fail a quiz Fail a quiz of a specific course Fail a quiz of the course %s Fail a specific quiz Fail the quiz %s GamiPress GamiPress - Tutor LMS integration GamiPress - Tutor LMS integration requires %s and %s in order to work. Please install and activate them. Pass a quiz of the course %s Pass the quiz %s Successfully pass a quiz Successfully pass a quiz of a specific course Successfully pass a specific quiz Tutor LMS Tutor LMS integration https://gamipress.com/ https://wordpress.org/plugins/gamipress-tutor-integration/ PO-Revision-Date: 2020-06-08 13:57:14+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha
Language: fr
Project-Id-Version: Plugins - GamiPress &#8211; Tutor LMS integration - Stable (latest release)
 Terminer un cours Terminer une leçon Terminer une leçon d’un cours spécifique Terminer une leçon du cours %s Terminer un quiz Terminer un cours spécifique Terminer une leçon spécifique Terminer un quiz spécifique Terminer n’importe quel quiz d’un cours spécifique Terminer n’importe quel quiz du cours %s Terminer le cours %s Terminer la leçon %s Terminer le quiz %s Connecter GamiPress avec Tutor LMS. S’inscrire à un cours S’inscrire à un cours spécifique S’inscrire au cours %s Échouer à un quiz Échouer à un quiz d’un cours spécifique Échouer à un quiz du cours %s Échouer à un quiz spécifique Échouer le quiz %s GamiPress GamiPress - Tutor LMS integration GamiPress - Tutor LMS integration nécessite %s et %s pour fonctionner. Veuillez les installer et les activer. Réussir un quiz du cours %s Réussir le quiz %s Réussir un quiz Réussir un quiz d’un cours spécifique Réussir un quiz spécifique Tutor LMS Tutor LMS integration https://gamipress.com/ https://wordpress.org/plugins/gamipress-tutor-integration/ 