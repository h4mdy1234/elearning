<?php
define( 'WP_CACHE', true ); // Added by WP Rocket

define( 'WP_CACHE', false /* Modified by NitroPack */ );
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'learning' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'w<QG|}BP^RHRt7 t-or+_}ECJdK]JO)ba_.:h=wyFx6z b4gcGjP8?d?X-0uu8mg' );
define( 'SECURE_AUTH_KEY',  'a0nG@p~01%k*2D:!mES,uDT!TnPUQE>hEO1Bv9pWNCi;fso?oWn]fBk2AmP(/wql' );
define( 'LOGGED_IN_KEY',    ',[$uq.-6ht{DkdvN.f-dP9`NUEILg9e4J?5 YJlq;jS?|yNw},3gw=NP 9rO!UzU' );
define( 'NONCE_KEY',        ' _0W#Ewjn=/-,Kpz!j|dkF<J;#L-*v5YM,A&jj<]y`)*l,<pcti/<X/p/>/rx1$.' );
define( 'AUTH_SALT',        'f51-#Rq[4cM5+cl%8pRgP2_f)}VTT3T!=`WF@af.J)G9!_}?NK$Yn9O-dc1avCq8' );
define( 'SECURE_AUTH_SALT', 'dX/.6{k6$~soB$u48^&CR_t@OUWW>F-vLR{{ipb:s_<*c.H0s-@]Fv1DkiVaC?UP' );
define( 'LOGGED_IN_SALT',   '-8?s#M%A`/#?=ACeI2 vu$ZPb]T,Wh3D?IG?q>0C|gqA=C}##7_,/%Msiw)_m<`H' );
define( 'NONCE_SALT',       'W3$_qc-=bG1/m440((-WfVV27Ns{zys7N(iw-P|Z@e/-Ti;qZH` NV9nDglPgM_k' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
